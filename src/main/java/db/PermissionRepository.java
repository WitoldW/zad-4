package db;

import domain.Permission;

import java.util.List;


public interface PermissionRepository extends Repository<Permission>  {
    public List<Permission> withPermission(String permission, PagingInfo page);
}
