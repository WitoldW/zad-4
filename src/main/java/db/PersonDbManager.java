package db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import domain.Person;

public class PersonDbManager {

    private Connection connection;
    private String url = "jdbc:hsqldb:hsql://localhost/workdb";

    private String createPersonTable =""
            + "CREATE TABLE Person("
            + "id bigint GENERATED BY DEFAULT AS IDENTITY,"
            + "name VARCHAR(20),"
            + "surname VARCHAR(50)"
            + ")";

    private String insertSql ="INSERT INTO person(name,lastName, phoneNumber) VALUES(?,?, ?)";
    private String selectSql ="SELECT * FROM person";
    private String deleteSql = "DELETE FROM person WHERE id=?";
    private String updateSql = "UPDATE person SET (name,lastName, phoneNumber)=(?,?,?) WHERE id=?";

    private PreparedStatement insert;
    private PreparedStatement select;
    private PreparedStatement delete;
    private PreparedStatement update;

    public PersonDbManager(){

        try {
            connection = DriverManager.getConnection(url);

            insert = connection.prepareStatement(insertSql);
            select = connection.prepareStatement(selectSql);
            delete = connection.prepareStatement(deleteSql);
            update = connection.prepareStatement(updateSql);
            ResultSet rs = connection.getMetaData().getTables(null, null, null, null);

            boolean tableExists =false;
            while(rs.next())
            {
                if(rs.getString("TABLE_NAME").equalsIgnoreCase("Person")){
                    tableExists=true;
                    break;
                }
            }

            if(!tableExists){
                Statement createTable = connection.createStatement();
                createTable.executeUpdate(createPersonTable);
            }


        } catch (SQLException e) {
            e.printStackTrace();
        }


    }

    public void add(Person person){

        try {
            insert.setString(1, person.getName());
            insert.setString(2, person.getLastName());
            insert.setString(3, person.getPhoneNumber());
            insert.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public void deleteById(int id){

        try {
            delete.setInt(1, id);
            delete.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public void update(Person p){

        try {
            update.setString(1, p.getName());
            update.setString(2, p.getLastName());
            update.setString(3, p.getPhoneNumber());
            update.setInt(4, p.getId());
            update.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }


    }

    public List<Person> getAll(){

        List<Person> result = new ArrayList<Person>();

        try {
            ResultSet rs = select.executeQuery();
            while(rs.next()){
                Person person = new Person(rs.getString("name"),rs.getString("lastName"),rs.getString("phoneNumber"));
                person.setId(rs.getInt("id"));
                result.add(person);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }


}
