package db;

import domain.Address;
import domain.Role;

import java.util.List;


public interface AddressRepository extends Repository<Address>  {
    public List<Address> withCity(String city, PagingInfo page);
    public List<Address> withStreet(String street, PagingInfo page);
    public List<Address> withZipCode(String zipCode, PagingInfo page);
}
