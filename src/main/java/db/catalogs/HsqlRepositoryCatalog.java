package db.catalogs;

import java.sql.Connection;

import db.*;
import db.repos.*;

public class HsqlRepositoryCatalog implements RepositoryCatalog {

    Connection connection;

    public HsqlRepositoryCatalog(Connection connection) {
        this.connection = connection;
    }

    public PersonRepository people() {
        return new HsqlPersonRepository(connection);
    }

    public RoleRepository role() {
        return new HsqlRoleRepository(connection);
    }

    public PermissionRepository permission() {
        return new HsqlPermissionRepository(connection);
    }

    public AddressRepository address() {
        return new HsqlAddressRepository(connection);
    }

    public UserRepository user() {
        return new HsqlUserRepository(connection);
    }

    public RolePermissionRepository rolePerm() {
        return new HsqlRolePermissiomRepository(connection);
    }

}
